﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowController : MonoBehaviour {
    public Rigidbody2D arrowPrefab;
    public Transform bowEnd;
    public float arrowSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Rigidbody2D arrowInstanse;
            arrowInstanse = Instantiate(arrowPrefab, bowEnd.position, bowEnd.rotation) as Rigidbody2D;
            arrowInstanse.AddForce(bowEnd.right * arrowSpeed * -1, ForceMode2D.Impulse);
        }
       
	}
}
