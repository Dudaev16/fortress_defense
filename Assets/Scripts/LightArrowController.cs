﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightArrowController : MonoBehaviour {
    Vector3 mousePosition;
    Vector3 mousePointer;
    float startTime;
    float TotalDistanceToDestination;
    public int damage = 10;
    // Use this for initialization
    void Start () {
        startTime = Time.time;
        TotalDistanceToDestination = Vector3.Distance(transform.position, mousePosition);
        mousePointer = Input.mousePosition;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
        
        mousePosition = Camera.main.ScreenToWorldPoint(mousePointer);
        float currentDuration = Time.time - startTime;
        float journeyFraction = currentDuration / TotalDistanceToDestination;
        transform.position = Vector3.Lerp(transform.position, mousePosition, journeyFraction);
    } 
}
