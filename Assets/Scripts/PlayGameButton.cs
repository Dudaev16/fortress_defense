﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayGameButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        
    }
    private void OnMouseDown()
    {
        StartCoroutine(ChangeButtonColor());
    }
    // Update is called once per frame
    void Update () {
 
	}
   IEnumerator ChangeButtonColor()
    {
        GetComponent<SpriteRenderer>().color = Color.cyan;
        yield return new WaitForSeconds(0.5f);
        GetComponent<SpriteRenderer>().color = Color.white;
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("FirstLevel", LoadSceneMode.Single);
    }
    public void LoadSceneByName()
    {

        
    }
}
