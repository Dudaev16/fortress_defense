﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FortressController : MonoBehaviour {
    public int fortressLife;
    int fortressLifePercent;
    public Sprite someCrashes;
    public Sprite halfHealth;
    public Sprite ruin;
    public GameObject gameOverPopup;
    internal SpriteRenderer spriteRenderer;
    bool gameOver = false;
    
    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        fortressLifePercent = fortressLife;
	}
	
	// Update is called once per frame
	void Update () {
        if (fortressLife <= (fortressLifePercent * 0.75))
        {
            GetComponent<SpriteRenderer>().sprite = someCrashes;
        }
        if (fortressLife <= (fortressLifePercent * 0.5))
        {
            GetComponent<SpriteRenderer>().sprite = halfHealth;
        }
        if (fortressLife <= 0)
        {
            GetComponent<SpriteRenderer>().sprite = ruin;
            gameOver = true;
            gameOverPopup.SetActive(true);
        }
        BlockGameOnPopup();
    }
    public void Restart()
    {

        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);

    }
    public void QuitToMainMenu()
    {

        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        
    }

    void BlockGameOnPopup()
    {
        if (gameOverPopup.activeInHierarchy)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
        
    }
}
