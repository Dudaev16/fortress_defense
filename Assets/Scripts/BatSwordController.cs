﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatSwordController : MonoBehaviour {
   // public Transform batDestinationPoint;
    public int health = 100;
    public int damage = 5;
    public float speed = 0.1f;
    float startTime;
    //float TotalDistanceToDestination;
    internal SpriteRenderer spriteRenderer;
    public AudioSource batDeathSound;
    bool shouldMove = true;
    bool stopIfArrowHit = false;
    Coroutine cor;
    // Use this for initialization
    void Start () {
        startTime = Time.time;
        //TotalDistanceToDestination = Vector3.Distance(transform.position, batDestinationPoint.position);
        spriteRenderer = GetComponent<SpriteRenderer>();
        cor = StartCoroutine(PauseMoveIfArrowHit());
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Arrow")
        {
            health -= FindObjectOfType<LightArrowController>().damage;
            StartCoroutine(ChangeColorOnArrowHit());
            stopIfArrowHit = true;
        }
        if (collision.tag == "Fortress")
        {
            StopCoroutine(cor);
            transform.Translate(new Vector2(0,0));
            shouldMove = false;     
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Fortress")
        {
            StopCoroutine(PauseMoveIfArrowHit());
            GetComponent<Animator>().SetTrigger("BatSwordAttack");
        }
    }
   
    
    // Update is called once per frame
    void Update () {
        // float currentDuration = Time.time - startTime;
        //float journeyFraction = currentDuration / TotalDistanceToDestination * 0.05f;
        //transform.position = Vector3.Lerp(transform.position, batDestinationPoint.position, journeyFraction);
        if (health <= 0)
        {
            GetComponent<Animator>().SetTrigger("BatIsDead");
            transform.Translate(new Vector2(0, 0));
            shouldMove = false;
        }

        //if (shouldMove)
        //{
        //    transform.Translate(speed, 0, Time.deltaTime);
        //} 
    }
    public void Attack()
    {     
        FindObjectOfType<FortressController>().fortressLife -= damage;
        GetComponent<AudioSource>().Play();
        StartCoroutine(FortressChangeColorOnAttack());
    }

    IEnumerator FortressChangeColorOnAttack()
    {
        FindObjectOfType<FortressController>().spriteRenderer.color = Color.green;
        yield return new WaitForSeconds(0.05f);
        FindObjectOfType<FortressController>().spriteRenderer.color = Color.white;
    }

    IEnumerator ChangeColorOnArrowHit()
    {
       gameObject.GetComponent<SpriteRenderer>().color = Color.red;       
        yield return new WaitForSeconds(0.02f);
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
    public void BatDeath()
    {
        StartCoroutine(WaitForDeathSoundPlay());

    }
    IEnumerator WaitForDeathSoundPlay()
    {
        batDeathSound.Play();
        yield return new WaitWhile(() => batDeathSound.isPlaying);
        Destroy(gameObject);
    }
    IEnumerator PauseMoveIfArrowHit()
    {
        while (!stopIfArrowHit)
        {
            transform.Translate(speed, 0, Time.deltaTime);
            yield return null;
        }
        if (stopIfArrowHit)
        {
            yield return new WaitForSeconds(3f);
            stopIfArrowHit = false;
        }
        while (!stopIfArrowHit)
        {
            transform.Translate(speed, 0, Time.deltaTime);
            yield return null;
        }
    } 
}
