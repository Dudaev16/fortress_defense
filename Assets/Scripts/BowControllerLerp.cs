﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowControllerLerp : MonoBehaviour {
    
    public Rigidbody2D arrowPrefab;
    public Transform bowEnd;
    // public Transform endPosition;
    
    

	// Use this for initialization
	void Start () {
        
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            GetComponent<Animator>().SetTrigger("ArcherShoot");
        }
	}

    public void Shoot()
    {
        Rigidbody2D arrowInstanse;
        arrowInstanse = Instantiate(arrowPrefab, bowEnd.position, bowEnd.rotation) as Rigidbody2D;
    }
}
