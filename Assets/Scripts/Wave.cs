﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave : MonoBehaviour {
    public string name;
    public Transform enemy;
    public int count;
    public float waveDelay;
}
