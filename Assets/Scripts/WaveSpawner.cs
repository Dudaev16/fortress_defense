﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaveSpawner : MonoBehaviour {
    public enum SpawnState { SPAWNING, WAITING, CONTINUING}
    public Wave[] waves;
    public GameObject WinGamePopup;
    private int nextWave = 0;

    public float timeBetweenWaves = 5f;
    private float delayBetweenSearchDeadEnemies = 1f;
    public float waveCountDown;

    public SpawnState state = SpawnState.CONTINUING;

	// Use this for initialization
	void Start () {
        waveCountDown = timeBetweenWaves;
	}
	
	// Update is called once per frame
	void Update () {
        if (state == SpawnState.WAITING)
        {
            // Check if enemies are still alive
            if (!EnemyIsAlive())
            {
                //Begin a new round
                Debug.Log("Wave Completed.");
                WaveCompleted();
            }
            else
            {
                return;
            }
        }
        if (waveCountDown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                //Start spawn waves...
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }

        BlockGameOnPopup();
    }
    bool EnemyIsAlive()
    {
        delayBetweenSearchDeadEnemies -= Time.deltaTime;

        if (delayBetweenSearchDeadEnemies <= 0f)
        {
            delayBetweenSearchDeadEnemies = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            } 
        }
        return true;
    }

    void WaveCompleted()
    {
        state = SpawnState.CONTINUING;
        waveCountDown = timeBetweenWaves;
        if (nextWave + 1 > waves.Length - 1)
        {
            Time.timeScale = 0;
            WinGamePopup.SetActive(true);
            
            //nextWave = 0;
        }
        else
        {
            nextWave++;
        }
       

    }
    
    
    IEnumerator SpawnWave(Wave _wave)
    {
        state = SpawnState.SPAWNING;
        //Spawn
        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.waveDelay);
        }
        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform enemy)
    {
        //Spawn enemy
        Instantiate(enemy, transform.position, transform.rotation);
    }

    //If win game, UI Controls

    public void Restart()
    {

        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);

    }
    public void QuitToMainMenu()
    {

        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);

    }

    void BlockGameOnPopup()
    {
        if (WinGamePopup.activeInHierarchy)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }
}
